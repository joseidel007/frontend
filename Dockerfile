#Node package is required
FROM node:13.12.0-alpine as build

#Create directory to app
WORKDIR /app

#Copy packages of the dependencies into directory
COPY package*.json ./

#Installing dependencies
RUN npm install

#Copy application in the same directory
COPY . .
RUN npm run build
# production environment
FROM nginx:stable-alpine
COPY --from=build /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

